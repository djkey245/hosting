<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126311365-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-126311365-1');
        </script>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Key Host - лучшый VPS хостинг в Украине</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!--<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700' rel='stylesheet' type='text/css'>-->
        <link rel="stylesheet" href="/css/fonticons.css">
        <link rel="stylesheet" href="/fonts/stylesheet.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <!--        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">-->


        <!--For Plugins external css-->
        <link rel="stylesheet" href="/css/plugins.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="/css/responsive.css" />

        <script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body data-spy="scroll" data-target="#navmenu" >
        <!--[if lt IE 8]>
        <![endif]-->
		<div id="app">
            <div class='preloader'><div class='loaded'>&nbsp;</div></div>
            <!--Home page style-->
            <header id="main_menu" class="header">
                <div class="main_menu_bg navbar-fixed-top">
                    <div class="container">
                        <div class="row">
                            <div class="nave_menu wow fadeInUp" data-wow-duration="1s">
                                <nav class="navbar navbar-default" id="navmenu">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a class="navbar-brand" href="#"><img src="/images/logo.png" alt=""/></a>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                                            <ul class="nav navbar-nav navbar-right">
                                                {{--<li class="active"><a href="#home">Главная</a></li>
                                                <li><a href="#pricing">VPS Хостинг</a></li>
                                                <li><a href="#sale">Заказать</a></li>
                                                <li><a href="#footer">Поддержка</a></li>--}}
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>

                    </div>

                </div>
            </header> <!--End of header -->



            <section name="home" id="home" class="home">
                <div class="home-overlay-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="main_slider_area">
                                <div class="slider">
                                    <div class="single_slider wow fadeIn" data-wow-duration="2s">
                                        <h2>Качественный хостинг и VPS</h2>
                                        <p>Мы любим свою работу...</p>
                                        <p>И работаем для ВАС...</p>
                                    </div>
                                    <!--<div class="single_slider">
                                        <h2>HELLO!</h2>
                                        <p>We love our work...</p>
                                        <p>And We're taking it seriously...</p>
                                    </div>
                                    <div class="single_slider">
                                        <h2>HELLO!</h2>
                                        <p>We love our work...</p>
                                        <p>And We're taking it seriously...</p>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End of Banner Section -->


            {{--<section id="register" class="register">
                <div class="container-fullwidth">
                    <div class="row text-center">
                        <div class="col-sm-6 col-xs-6 no-padding">
                            <div class="single_register single_login">
                                <a href="">Вход</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-6 no-padding">
                            <div class="single_register">
                                <a href="">Регистрация</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>--}}





            <section id="pricingtop" class="pricingtop">
                <div class="overlay">
                    <div class="container">
                        <div class="main_pricingtop"></div>
                    </div>
                </div>
            </section>

            <section id="pricing" class="pricing">
                <div class="container">
                    <div class="row">
                        <div class="main_pricing">

                            <div class="pricing_content">


                                <div class="col-md-4 col-sm-6">
                                    <div class="single_pricing pricing_color_two wow fadeIn" data-wow-duration="1.2s">
                                        <div class="pricing_head text-center">
                                            <div class="icon_area p_icon_two">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <h3>VPS SSD 1</h3>
                                            <div class="separator"></div>
                                        </div>
                                        <figure class="single_pricing_figure">
                                            <ul>
                                                <li><span class="princing_name">Процессор </span> <span class="princing_price princing_price_two">1 ядро</span></li>
                                                <li><span class="princing_name"></span> <span class="princing_price princing_price_two"> 2.4GHz</span></li>
                                                <li><span class="princing_name">Оперативная память</span> <span class="princing_price princing_price_two">2 GB</span></li>
                                                <li><span class="princing_name">SSD </span> <span class="princing_price princing_price_two">20 GB</span></li>
                                            </ul>
                                            <div class="separator"></div>
                                        </figure>

                                        <footer class="pricing_footer text-center">
                                            <h3> 199 <span class="dolor">грн.</span> <span class="month">/месяц </span></h3>
                                            <div class="smallseparator separetor_two"></div>
                                            <a href="#sale">Выбрать план</a>
                                        </footer>

                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="single_pricing pricing_color_three wow fadeIn" data-wow-duration="1.8s">
                                        <div class="pricing_head text-center">
                                            <div class="icon_area p_icon_three">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <h3>VPS SSD 2</h3>
                                            <div class="separator"></div>
                                        </div>
                                        <figure class="single_pricing_figure">
                                            <ul>
                                                <li><span class="princing_name">Процессор </span> <span class="princing_price princing_price_two">1 ядро</span></li>
                                                <li><span class="princing_name"></span> <span class="princing_price princing_price_two"> 2.4GHz</span></li>
                                                <li><span class="princing_name">Оперативная память</span> <span class="princing_price princing_price_two">4 GB</span></li>
                                                <li><span class="princing_name">SSD </span> <span class="princing_price princing_price_two">40 GB</span></li>
                                            </ul>
                                            <div class="separator"></div>
                                        </figure>

                                        <footer class="pricing_footer text-center">
                                            <h3>399 <span class="dolor">грн.</span>  <span class="month">/месяц </span></h3>
                                            <div class="smallseparator separetor_three"></div>
                                            <a href="#sale">Выбрать план</a>
                                        </footer>

                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="single_pricing pricing_color_four wow fadeIn" data-wow-duration="2.3s">
                                        <div class="pricing_head text-center">
                                            <div class="icon_area p_icon_four">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <h3>VPS SSD 3</h3>
                                            <div class="separator"></div>
                                        </div>

                                        <figure class="single_pricing_figure">
                                            <ul>
                                                <li><span class="princing_name">Процессор </span> <span class="princing_price princing_price_two">2 ядра</span></li>
                                                <li><span class="princing_name"></span> <span class="princing_price princing_price_two"> 2.4GHz</span></li>
                                                <li><span class="princing_name">Оперативная память</span> <span class="princing_price princing_price_two">8 GB</span></li>
                                                <li><span class="princing_name">SSD </span> <span class="princing_price princing_price_two">80 GB</span></li>
                                            </ul>
                                            <div class="separator"></div>
                                        </figure>

                                        <footer class="pricing_footer text-center">
                                            <h3>999 <span class="dolor">грн.</span>  <span class="month">/месяц </span></h3>
                                            <div class="smallseparator separetor_four"></div>
                                            <a href="#sale">Выбрать план</a>
                                        </footer>
                                    </div>
                                </div>

                            </div><!-- End of pcining section -->

                        </div>
                    </div>
                    <div class="row">
                        <div class="main_pricing">

                            <div class="pricing_content">


                                <div class="col-md-4 col-sm-6">
                                    <div class="single_pricing pricing_color_two wow fadeIn" data-wow-duration="1.2s">
                                        <div class="pricing_head text-center">
                                            <div class="icon_area p_icon_two">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <h3>Хостинг - Начало</h3>
                                            <div class="separator"></div>
                                        </div>
                                        <figure class="single_pricing_figure">
                                            <ul>
                                                <li><span class="princing_name">Сайты </span> <span class="princing_price princing_price_two">1</span></li>
                                                <li><span class="princing_name">Место под почту</span> <span class="princing_price princing_price_two">неогранич.</span></li>
                                                <li><span class="princing_name">Место на SSD диске </span> <span class="princing_price princing_price_two">1GB</span></li>
                                                <li><span class="princing_name">PHP memory_limit </span> <span class="princing_price princing_price_two">64МB</span></li>
                                            </ul>
                                            <div class="separator"></div>
                                        </figure>

                                        <footer class="pricing_footer text-center">
                                            <h3> 40 <span class="dolor">грн.</span> <span class="month">/месяц </span></h3>
                                            <div class="smallseparator separetor_two"></div>
                                            <a href="#sale">Выбрать план</a>
                                        </footer>

                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="single_pricing pricing_color_three wow fadeIn" data-wow-duration="1.8s">
                                        <div class="pricing_head text-center">
                                            <div class="icon_area p_icon_three">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <h3>Хостинг - Профессиональный</h3>
                                            <div class="separator"></div>
                                        </div>
                                        <figure class="single_pricing_figure">
                                            <ul>
                                                <li><span class="princing_name">Сайты </span> <span class="princing_price princing_price_two">5</span></li>
                                                <li><span class="princing_name">Место под почту</span> <span class="princing_price princing_price_two">неогранич.</span></li>
                                                <li><span class="princing_name">Место на SSD диске </span> <span class="princing_price princing_price_two">5GB</span></li>
                                                <li><span class="princing_name">PHP memory_limit </span> <span class="princing_price princing_price_two">128МB</span></li>
                                            </ul>
                                            <div class="separator"></div>
                                        </figure>

                                        <footer class="pricing_footer text-center">
                                            <h3>80 <span class="dolor">грн.</span>  <span class="month">/месяц </span></h3>
                                            <div class="smallseparator separetor_three"></div>
                                            <a href="#sale">Выбрать план</a>
                                        </footer>

                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="single_pricing pricing_color_four wow fadeIn" data-wow-duration="2.3s">
                                        <div class="pricing_head text-center">
                                            <div class="icon_area p_icon_four">
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <h3>Хостинг - Премиум</h3>
                                            <div class="separator"></div>
                                        </div>

                                        <figure class="single_pricing_figure">
                                            <ul>
                                                <li><span class="princing_name">Сайты </span> <span class="princing_price princing_price_two">неогранич.</span></li>
                                                <li><span class="princing_name">Место под почту</span> <span class="princing_price princing_price_two">неогранич.</span></li>
                                                <li><span class="princing_name">Место на SSD диске </span> <span class="princing_price princing_price_two">10GB</span></li>
                                                <li><span class="princing_name">PHP memory_limit </span> <span class="princing_price princing_price_two">256МB</span></li>
                                            </ul>
                                            <div class="separator"></div>
                                        </figure>

                                        <footer class="pricing_footer text-center">
                                            <h3>160 <span class="dolor">грн.</span>  <span class="month">/месяц </span></h3>
                                            <div class="smallseparator separetor_four"></div>
                                            <a href="#sale">Выбрать план</a>
                                        </footer>
                                    </div>
                                </div>

                            </div><!-- End of pcining section -->

                        </div>
                    </div>

                </div>
            </section><!-- End of Pricing Section -->





            <section id="service" class="service">
                <div class="container">
                    <div class="row">
                        <div class="main_service text-center">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single_service wow fadeIn" data-wow-duration=".6s">
                                    <div class="single_service_icon icon_one">
                                        <a href=""><i class="fa fa-flash"></i></a>
                                    </div>
                                    <div class="single_service_deatels">
                                        <h4>Отличные сервера</h4>
                                        <p>Все сервера оснащены новыми процессорами и SSD.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="single_service wow fadeIn" data-wow-duration="1.7s">
                                    <div class="single_service_icon icon_three">
                                        <a href=""><i class="fa fa-arrows"></i></a>
                                    </div>
                                    <div class="single_service_deatels">
                                        <h4>Продажа доменов</h4>
                                        <p>Купить у нас домен легко.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="single_service wow fadeIn" data-wow-duration="2s">
                                    <div class="single_service_icon icon_four">
                                        <a href=""><i class="fa fa-comments"></i></a>
                                    </div>
                                    <div class="single_service_deatels">
                                        <h4>Поддержка</h4>
                                        <p>Что-то не работает - пишите нам, исправим!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End of service Section -->
            <hr>

            <main-component></main-component>

            <section id="footer" class="footer sections">
                <div class="container">
                    <div class="row">
                        <div class="main_footer wow fadeInDown" data-wow-duration="2s">
                            <div class="col-sm-3 col-xs-12">
                                <div class="footer_logo">
                                    <a href=""><img src="/images/logo.png" alt="" /></a>
                                </div>
                            </div>
                            <div class="col-sm-9 col-xs-12">
                                <div class="footer_menu">
                                    <ul class="list-inline">
                                        <li>+38 (068) 699-22-45</li>
                                        <li>djkey245@gmail.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>







                </div>
            </section><!-- End of footer3 section -->


        </div>






        <!-- STRAT SCROLL TO TOP -->

        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>

        <script src="/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/js/jquery.easypiechart.min.js"></script>

        <script src="/js/plugins.js"></script>
        <script src="/js/main.js"></script>
        <script src="/js/app.js"></script>

    </body>
</html>

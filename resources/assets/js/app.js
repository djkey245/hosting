import Vue from "vue";
import VueResource from "vue-resource"

import MainComponent from "./components/main"

Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
Vue.http.options.emulateJSON = true;

export const Events = new Vue({});

const app = new Vue({
    el: '#app',

    components: {
        MainComponent,
    }
});

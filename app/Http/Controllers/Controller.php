<?php

namespace App\Http\Controllers;

use App\Bid;
use App\Http\Requests\EmailRequest;
use App\Mail\ToEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;




    public function send(EmailRequest $request){


        $data =  $request->only(['email','phone','type','comment']);

        Bid::create($data);
        return response()->json();


    }
}

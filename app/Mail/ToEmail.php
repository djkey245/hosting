<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ToEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $items;
    public function __construct($items)
    {
        $this->items = $items;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Message from HOST-KEY.PP.UA')
            ->view('emails.mail', ['items' => $this->items]);
        /*return $this->from('support@key-host.pp.ua')
            ->to('djkey245@gmail.com')
            ->view('email.mail')
            ->with(['items' => $this->items]);*/
    }
}
